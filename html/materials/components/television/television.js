var Television = function() {

	var comp = $('[data-component="television"]'), guideComp = $('[data-component="guide"]');
	var positions = {}, channelsLength = comp.find('.channel').length;

	this.init = function() {

		prepareTelevision();
		addHandlers();

	}

	function addHandlers() {

		var dataChannel, dataToggle, channelsLength, channelIndex;

		$('.guide-toggle').on('click', function() {
			dataToggle = $(this).data('toggle');
			toggleGuide(dataToggle);
		});

		$('.channel-thumb').on('click', function() {
			dataChannel = $(this).data('channel');
			changeChannel(dataChannel);
		});

		$('.next-channel').on('click', function() {
			dataChannel = $('.active').next().data('channel');
			if (dataChannel === undefined) {
				dataChannel = $('.channel-thumb:first-child').data('channel');
			}
			changeChannel(dataChannel);
		});

		$('.prev-channel').on('click', function () {
			dataChannel = $('.active').prev().data('channel');
			if (dataChannel === undefined) {
				dataChannel = $('.channel-thumb:last-child').data('channel');
			}
			changeChannel(dataChannel);
		});

	}

	function changeChannel(index) {
		$('[data-channel]').removeClass('active');
		$('[data-channel="' + index + '"]').addClass('active');
	}

	function toggleGuide(data) {

		if (data === 'closed') {
			positions.btnBottom = 230;
			positions.guideBottom = 0;
			data = 'open';
		} else {
			positions.btnBottom = 30;
			positions.guideBottom = '-100%';
			data = 'closed';
		}
		$('.guide-toggle').data('toggle', data);
		TweenMax.to('.guide-toggle', 0.5, { bottom: positions.btnBottom, ease: Quint.easeOut });
		TweenMax.to(guideComp, 0.5, { bottom: positions.guideBottom, ease: Quint.easeOut });

	}

	function prepareTelevision() {
		comp.find('.channel:first-child').addClass('active');
		guideComp.find('.channel-thumb:first-child').addClass('active');
	}

	return this.init();

}

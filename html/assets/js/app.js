var isMobile = false;
var isTablet = false;
var windowWidth = 0;

$(document).ready(function(){

	checkWindow();

	if ($('input').length) {
		var forms = new Forms();
	}

	if ($('[data-equalizer]').length) {
		var equalizer = new Equalizer();
	}

	if ($('[data-component="television"]').length) {
		var television = new Television();
	}

});

function checkWindow() {
	windowWidth = $(window).width();

	if (windowWidth >= 768 && windowWidth <= 992) {
		isTablet = true;
	}

	if (windowWidth <= 767) {
		isMobile = true;
	}
}

var Equalizer = function() {

	var data, prevData, height = 0, counter = 0, dataCount = 0;

	calculateEqualizer = function() {
		$('[data-equalizer]').each(function (e) {
			data = $(this).attr('data-equalizer');
			if (counter < 1) {
				prevData = data;
				counter++;
			}
			if (data === prevData) {
				dataCount++;
				calculateHeight($(this), data);
			} else {
				counter = 0;
				height = 0;
				prevData = '';
				calculateHeight($(this), data);
			}
			if (dataCount === $('[data-equalizer="' + data + '"]').length) {
				$('[data-equalizer="' + data + '"]').height(height);
				dataCount = 0;
			}
		});
	}

	function calculateHeight(item, data) {
		if (item.height() > height) {
			height = item.height();
		}
	}

	return calculateEqualizer();

}
